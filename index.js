/**
 * @format
 */
import React from 'react';
import { AppRegistry } from 'react-native';
import AppNavigation from './src/navigation/AppNavigation';
import { Provider } from 'react-redux';
import store from './src/redux/store';
import { name as appName } from './app.json';
const App = () =>
    <Provider store={store}>
        <AppNavigation />
    </Provider>
AppRegistry.registerComponent(appName, () => App);
