import {deviceStorage} from '../services/deviceStorage.service';

const tokenKey = 'token';

const getToken = () => {
  return deviceStorage.getItem(tokenKey);
};

const setToken = token => {
  return deviceStorage.setItem(tokenKey, token);
};
const removeToken = () => {
  return deviceStorage.removeItem(tokenKey);
};

export {getToken, setToken, removeToken};
