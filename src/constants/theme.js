import {Dimensions, Platform} from 'react-native';
// const { width, height } = Dimensions.get('window');
const statusBarMargin = Platform.OS != 'ios' ? 30 : 0;
const DIMENSIONS = {
  WINDOW_WIDTH: Dimensions.get('window').width,
  WINDOW_HEIGHT: Dimensions.get('window').height,
};

// COLOR
const colors = {
  PRIMARY: '#3CB371',
  SECONDARY: '#CA3F27',

  blue: '#4066B5',
  lightblue: 'rgba(46,92,255,0.2)',
  green: '#0DB665',
  red: '#FF5033',
  yellow: '#F7C137',
  teal: '#00C1D4',
  purple: '#8C54FF',
  black: '#2E384D',
  black2: '#69707F',
  black3: '#8798AD',
  white: '#FFFFFF',
  gray: '#BFC5D2',
  gray2: '#F4F6FC',
  gray3: '#EEF3F5',
  gray4: '#A6A6A6',
  caption: '#B0BAC9',
  input: 'rgba(224, 231, 255, 0.1)', // '#E0E7FF' 20%
  border: '#D6DDF6',
  card: 'rgba(46,91,255,0.08)',
  shadow: 'rgba(46,91,255,0.07)',
};

const tintColor = {
  active: colors.white,
  activeSecondary: colors.green,
  inactive: colors.gray,
  inactiveSecondary: colors.black,
};

//FONTS

// TYPOGRAPHY
const scalingFactors = {
  xxs: 35,
  xs: 30,
  sm: 25,
  md: 15,
  lg: 10,
  xlg: 7,
};

const primaryFontFamily = 'BalooDa-Regular';
const fontSizes = {
  h1: {
    fontSize: DIMENSIONS.WINDOW_WIDTH / scalingFactors.xlg,
    lineHeight: (DIMENSIONS.WINDOW_WIDTH / scalingFactors.xlg) * 1.3,
  },
  h2: {
    fontSize: DIMENSIONS.WINDOW_WIDTH / scalingFactors.lg,
    lineHeight: (DIMENSIONS.WINDOW_WIDTH / scalingFactors.lg) * 1.3,
  },
  h3: {
    fontSize: DIMENSIONS.WINDOW_WIDTH / scalingFactors.md,
    lineHeight: (DIMENSIONS.WINDOW_WIDTH / scalingFactors.md) * 1.3,
  },
  h4: {
    fontSize: DIMENSIONS.WINDOW_WIDTH / scalingFactors.sm,
    lineHeight: (DIMENSIONS.WINDOW_WIDTH / scalingFactors.sm) * 1.3,
  },
  h5: {
    fontSize: DIMENSIONS.WINDOW_WIDTH / scalingFactors.xs,
    lineHeight: (DIMENSIONS.WINDOW_WIDTH / scalingFactors.xs) * 1.3,
  },
  h6: {
    fontSize: DIMENSIONS.WINDOW_WIDTH / scalingFactors.xxs,
    lineHeight: (DIMENSIONS.WINDOW_WIDTH / scalingFactors.xxs) * 1.3,
  },
};
const fonts = {
  caption: {
    sm: {
      fontFamily: primaryFontFamily,
      fontSize: fontSizes.h6.fontSize,
      color: colors.white,
    },
    md: {
      fontFamily: primaryFontFamily,
      fontSize: fontSizes.h4.fontSize,
      color: colors.white,
    },
    lg: {
      fontFamily: primaryFontFamily,
      fontSize: fontSizes.h2.fontSize,
      color: colors.white,
    },
    xlg: {
      fontFamily: primaryFontFamily,
      fontSize: fontSizes.h1.fontSize,
      color: colors.white,
    },
    logo: {
      fontFamily: primaryFontFamily,
      fontSize: DIMENSIONS.WINDOW_WIDTH / 5.5,
    },
  },
  paragraph: {
    sm: {
      fontSize: fontSizes.h6.fontSize,
      color: colors.black,
    },
    md: {
      fontSize: fontSizes.h4.fontSize,
      color: colors.black,
    },
    lg: {
      fontSize: fontSizes.h1.fontSize,
      color: colors.black,
    },
  },

  button: {
    fontFamily: primaryFontFamily,
    fontSize: fontSizes.h4.fontSize,
    color: colors.white,
  },
};
const shadows = {
  low: {
    shadowColor: 'rgba(0, 0, 0, 1)',
    shadowOpacity: 0.6,
    elevation: 6,
    shadowRadius: 25,
    shadowOffset: {width: 1, height: 13},
  },
};
const borderRadiuses = {
  xxs: {borderRadius: 5},
  xs: {borderRadius: 10},
  sm: {borderRadius: 15},
  md: {borderRadius: 20},
  lg: {borderRadius: 25},
  xlg: {borderRadius: 30},
};
const backgroundColors = {
  blue: {
    backgroundColor: colors.blue,
  },
  red: {
    backgroundColor: colors.red,
  },
  green: {
    backgroundColor: colors.green,
  },
  default: {
    backgroundColor: colors.gray,
  },
};

//ICONS
const icons = {
  exploreIcon: require('../assets/icons/navigation_icons/2explore-icon.png'),
  profileIcon: require('../assets/icons/navigation_icons/2profile-icon.png'),
  activityIcon: require('../assets/icons/navigation_icons/2heart-icon.png'),
  dashboardIcon: require('../assets/icons/navigation_icons/2star-icon.png'),
  addIcon: require('../assets/icons/navigation_icons/plus-icon.png'),
  settingsIcon: require('../assets/icons/navigation_icons/settings-icon.png'),

  cameraIcon: require('../assets/icons/camera-icon.png'),

  mailIcon: require('../assets/icons/mail-icon.png'),
  userIcon: require('../assets/icons/username-icon.png'),
  passwordIcon: require('../assets/icons/password-icon.png'),
  errorIcon: require('../assets/icons/error-icon.png'),
  gridPhotoIcon: require('../assets/icons/grid-photo-icon.png'),
  timelinePhotoIcon: require('../assets/icons/timeline-photo-icon.png'),
};
const iconSizes = {
  width: 24,
  height: 24,
};

export {
  colors,
  primaryFontFamily,
  fontSizes,
  fonts,
  icons,
  iconSizes,
  shadows,
  borderRadiuses,
  backgroundColors,
  statusBarMargin,
  DIMENSIONS,
  tintColor,
};
