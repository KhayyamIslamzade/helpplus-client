const images = {
    noImage: require('../assets/images/noimage.png'),
    userImage: require('../assets/images/userimage.jpg'),
}

export {
    images
}
