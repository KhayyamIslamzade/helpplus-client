const registerRules = {
  email: {
    presence: true,
    format: {
      pattern: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
      message: '^is invalid',
    },
  },
  userName: {
    presence: true,
    format: {
      pattern: /^(?!.*\.\.)(?!.*\.$)[^\W][\w.]{6,30}$/,
      message: '^ Username is not in valid format',
    },
    length: {
      minimum: 6,
      maximum: 30,
      message: '^Invalid Username',
    },
  },
  password: {
    presence: true,
    length: {
      minimum: 6,
      maximum: 100,
      message: '^Invalid Password',
    },
  },
  confirmPassword: {
    presence: true,
    equality: {
      attribute: 'password',
      message: '^Passwords do not match',
    },
    length: {
      minimum: 6,
      maximum: 100,
      message: '^Invalid Password',
    },
  },
};

export {registerRules};
