import AsyncStorage from '@react-native-community/async-storage';

saveItem = async (key, value) => {
  console.log('SAVE ITEM');
  try {
    await AsyncStorage.setItem(key, value);
  } catch (error) {
    console.log('AsyncStorage Error: ' + error.message);
  }
};
getItem = async key => {
  try {
    const value = await AsyncStorage.getItem(key);
    if (value !== null) {
      return value;
    }
  } catch (error) {
    console.log('AsyncStorage Error: ' + error.message);
  }
};
multiGet = async (array = Array) => {
  try {
    const value = await AsyncStorage.multiGet(array);
    if (value !== null) {
      return value;
    }
  } catch (error) {
    console.log('AsyncStorage Error: ' + error.message);
  }
};
setItem = async (key, variable) => {
  try {
    const value = await AsyncStorage.setItem(key, variable);
    if (value !== null) {
      return value;
    }
  } catch (error) {
    console.log('AsyncStorage Error: ' + error.message);
  }
};
removeItem = async key => {
  try {
    await AsyncStorage.removeItem(key);
  } catch (error) {
    console.log('AsyncStorage Error: ' + error.message);
  }
};
multiRemove = async (array = Array) => {
  try {
    console.log('multi remove');
    await AsyncStorage.multiRemove(array);
  } catch (error) {
    console.log('AsyncStorage Error: ' + error.message);
  }
};

export const deviceStorage = {
  saveItem,
  getItem,
  setItem,
  multiGet,
  multiRemove,
};
