import {axios} from '../helpers/config';
import {POST_URLS} from '../helpers/urls';
import {handleError, handleResponse} from '../helpers/responseHandler';
// import deviceStorage from './deviceStorage.service'

add = post => {
  const {image, caption} = post;
  if (image.uri && image.fileName && image.type && caption)
    handleError('data is not valid');

  var data = new FormData();
  data.append('image', image);
  data.append('caption', caption);
  return axios.post(POST_URLS.ADD_URL, data, {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
  });
};
getPosts = () => {
  return axios.get(POST_URLS.FETCH_URL);
};
export const postService = {
  add,
  getPosts,
};
