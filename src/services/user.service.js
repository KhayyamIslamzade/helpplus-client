import {axios} from '../helpers/config';
import {AUTHORIZATION_URLS} from '../helpers/urls';
import {handleError, handleResponse} from '../helpers/responseHandler';

login = (emailOrUsername, password) => {
  console.log(
    'TCL: login -> AUTHORIZATION_URLS.LOGIN_URL',
    AUTHORIZATION_URLS.LOGIN_URL,
  );
  return axios.post(AUTHORIZATION_URLS.LOGIN_URL, {emailOrUsername, password});
};

register = user => {
  return axios.post(AUTHORIZATION_URLS.REGISTER_URL, user);
};

logout = () => {};

export const userService = {
  register,
  login,
  logout,
};
