import React from 'react';
import {createStackNavigator} from 'react-navigation-stack';
import {icons} from '../../constants/theme';
import ProfileNavigation from '../Home/ProfileNavigation';
import SettingsButton from '../../components/SettingsButton';
import {Explore, Dashboard, NewPost} from '../../screens';

export const stackNavigator = {
  ExploreStack: createStackNavigator({
    Explore,
  }),
  NewPostStack: createStackNavigator({
    NewPost,
  }),
  DashboardStack: createStackNavigator({
    Dashboard,
  }),
  ProfileStack: createStackNavigator({
    ProfileTopTab: {
      screen: ProfileNavigation,
      navigationOptions: () => {
        return {
          headerStyle: {
            elevation: 0, // remove shadow on Android
            shadowOpacity: 0, // remove shadow on iOS
          },

          headerRight: <SettingsButton iconPath={icons.settingsIcon} />,
        };
      },
    },
  }),
};
