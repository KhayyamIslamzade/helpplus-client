import React from 'react';
import {View} from 'react-native';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import AddPost from '../../components/AddPost';
import TabBar from '../../components/TabBar';
import {Activity} from '../../screens';
import {stackNavigator} from './TopTabNavigation';

// import { Explore, Dashboard, Adding, Profile, Activity } from '../../helpers/screenNames'

export default navigation = createBottomTabNavigator(
  {
    Dashboard: {
      screen: stackNavigator.DashboardStack,
    },
    Explore: {
      screen: stackNavigator.ExploreStack,
    },
    AddPost: {
      //   screen: stackNavigator.NewPostStack, // Empty screen
      screen: () => null, // Empty screen
    },
    Activity,
    Profile: {
      screen: stackNavigator.ProfileStack,
    },
  },
  {
    order: ['Dashboard', 'Explore', 'AddPost', 'Activity', 'Profile'],
    initialRouteName: 'Profile',
    tabBarPosition: 'bottom',
    swipeEnabled: true,
    animationEnabled: true,

    tabBarComponent: TabBar,

    // tabBarOptions: {
    //   activeTintColor: '#222',
    //   inactiveTintColor: '#ADADAD',
    //   showLabel: false,
    //   style: {
    //     backgroundColor: '#fff',
    //     padding: -10,
    //   },
    // },
    // defaultNavigationOptions: ({navigation}) => ({
    //   tabBarIcon: ({focused, tintColor}) => {
    //     const {routeName} = navigation.state;

    //     let source = '';
    //     switch (routeName) {
    //       case 'Profile':
    //         source = icons.profileIcon;
    //         break;
    //       case 'Activity':
    //         source = icons.activityIcon;
    //         break;
    //       case 'Explore':
    //         source = icons.exploreIcon;
    //         break;
    //       case 'Dashboard':
    //         source = icons.dashboardIcon;
    //         break;
    //       default:
    //         source = icons.profileIcon;
    //         break;
    //     }
    //     return (
    //       <Image
    //         focused={focused}
    //         source={source}
    //         style={{
    //           resizeMode: 'stretch',
    //           width: iconSizes.width,
    //           height: iconSizes.height,
    //           tintColor: tintColor,
    //         }}
    //       />
    //     );
    //   },
    // }),
  },
);
