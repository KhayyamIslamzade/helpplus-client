
import { createAppContainer } from 'react-navigation';
import navigation from './BottomTabNavigation'
export default App = createAppContainer(navigation)
