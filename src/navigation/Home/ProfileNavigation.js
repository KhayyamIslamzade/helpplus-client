import React from 'react'
import {
    Text,
    View,
} from 'react-native'

import { createDrawerNavigator } from 'react-navigation-drawer';
import {
    Settings,
    Profile
} from '../../screens';


export default ProfileDrawerNavigation = createDrawerNavigator(
    {
        Profile,
    },
    {
        drawerPosition: 'right',
        contentComponent: Settings

    }
);
