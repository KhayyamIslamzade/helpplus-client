import React from 'react';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import AuthNavigation from './Main/AuthNavigation';
import BottomTabNavigation from './Home/BottomTabNavigation';
import {stackNavigator} from './Home/TopTabNavigation';
import {AuthLoading as AuthLoadingScreen} from '../screens/index';
// import {AnimateTest} from '../screens/index';
import {SpinnerScratch} from '../screens/index';

export default createAppContainer(
  createSwitchNavigator({
    // You could add another route here for authentication.
    // Read more at https://reactnavigation.org/docs/en/auth-flow.html
    AuthLoading: AuthLoadingScreen,
    Auth: AuthNavigation,
    Main: createStackNavigator(
      {
        TabNavigation: {
          screen: BottomTabNavigation,
        },
        NewPost: {
          screen: stackNavigator.NewPostStack,
        },
      },
      {
        headerMode: 'none',
        mode: 'modal',
      },
    ),
  }),
);
