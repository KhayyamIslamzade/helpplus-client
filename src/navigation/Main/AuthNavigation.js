import React from 'react'
import { createStackNavigator } from 'react-navigation-stack';
import { Login, Main, Register } from '../../screens';

export default createStackNavigator({
    Main,
    Register,
    Login,
},
    {
        defaultNavigationOptions: {
            header: null
        }
    });