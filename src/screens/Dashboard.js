import React, {Component} from 'react';
import {Text, View, StatusBar, SafeAreaView, StyleSheet} from 'react-native';
import {statusBarMargin} from '../constants/theme';

export default class DashboardScreen extends Component {
  render() {
    return (
      <SafeAreaView style={styles.container} forceInset={{top: 'never'}}>
        <StatusBar backgroundColor="transparent" barStyle="dark-content" />
        <View style={{flex: 1, opacity: 0.2, backgroundColor: 'gray'}}></View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // marginTop: statusBarMargin
  },
});
