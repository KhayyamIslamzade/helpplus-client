import React, {Component} from 'react';
import {Text, View, StyleSheet} from 'react-native';

export default class ExploreScreen extends Component {
  render() {
    return (
      <View style={{flex: 1, opacity: 0.2, backgroundColor: 'gray'}}>
        <Text>Explore</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({});
