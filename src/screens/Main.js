import React, {Component} from 'react';
import {
  View,
  Image,
  ImageBackground,
  SafeAreaView,
  Dimensions,
  StatusBar,
  StyleSheet,
} from 'react-native';
import {Button, Text, Block} from '../components/core';
import {TouchableOpacity} from 'react-native-gesture-handler';
import * as theme from '../constants/theme';
const {width} = Dimensions.get('window');

export default class Main extends Component {
  render() {
    const {navigation} = this.props;
    return (
      <SafeAreaView style={{flex: 1}}>
        <StatusBar translucent backgroundColor="transparent" />
        <ImageBackground
          accessibilityRole={'image'}
          resizeMethod={'auto'}
          source={require('../assets/images/background-v2.png')}
          style={styles.background}
          imageStyle={styles.backgroundImage}>
          <Block flex={3} style={{paddingTop: 20, paddingLeft: 30}}>
            <Text caption="xlg" style={{marginBottom: -40}}>
              Welcome
            </Text>
            <Text caption="xlg" style={{marginBottom: -20}}>
              to Help+
            </Text>
            <Text caption="sm" opacity={0.7}>
              The Good Side of Social Media
            </Text>
          </Block>
          <Block flex={3} center>
            <Text caption="logo" color="white" style={styles.logoShadow}>
              h+
            </Text>
          </Block>
          <Block right center flex={4} style={{marginBottom: 20}}>
            <Button
              full
              backgroundColor="blue"
              borderRadius="lg"
              shadow="low"
              style={{
                height: 55,
                marginVertical: 5,
                paddingVertical: 15,
              }}>
              <Block row>
                <Block flex={1} middle center style={styles.verticalSeperator}>
                  <Image
                    source={require('../assets/icons/facebook-logo.png')}
                    style={{
                      width: 30,
                      height: 30,
                      resizeMode: 'stretch',
                      tintColor: '#FFF',
                    }}
                  />
                </Block>
                <Block flex={4} middle center>
                  <Text button center style={styles.iconText}>
                    Continue with Facebook
                  </Text>
                </Block>
              </Block>
            </Button>
            <Button
              full
              backgroundColor="red"
              borderRadius="lg"
              shadow="low"
              style={{
                height: 55,
                marginVertical: 5,
                paddingVertical: 15,
              }}
              onPress={() => navigation.navigate('Dashboard')}>
              <Block row>
                <Block flex={1} middle center style={styles.verticalSeperator}>
                  <Image
                    source={require('../assets/icons/google-logo.png')}
                    style={{
                      width: 30,
                      height: 30,
                      resizeMode: 'stretch',
                      tintColor: '#FFF',
                    }}
                  />
                </Block>

                <Block flex={4} middle center>
                  <Text button center style={styles.iconText}>
                    Continue with Google
                  </Text>
                </Block>
              </Block>
            </Button>
            <View style={[styles.btnSeperator, {flexDirection: 'row'}]}>
              <View
                style={{
                  backgroundColor: 'black',
                  height: 1,
                  borderRadius: 20,
                  flex: 1,
                  alignSelf: 'center',
                }}
              />
            </View>
            <Button
              full
              backgroundColor="green"
              borderRadius="lg"
              shadow="low"
              style={{
                height: 55,
                marginVertical: 5,
                paddingVertical: 15,
              }}
              onPress={() => navigation.navigate('Register')}>
              <Text button center h3>
                Sign Up
              </Text>
              {/* <Text h3 center>Sign Up</Text> */}
            </Button>

            <Block row center middle>
              <Text center paragraph="sm" style={{color: '#FFFFFFBB'}}>
                Already have an account?
              </Text>
              <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                <Text style={styles.loginTextParagraph}> Login </Text>
              </TouchableOpacity>
            </Block>
            {/* <Text

                            style={{ marginTop: 10 }}>


                        </Text> */}
          </Block>
        </ImageBackground>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  background: {
    width: '100%',
    height: '100%',
  },
  backgroundImage: {
    resizeMode: 'stretch',
    alignSelf: 'flex-start',
  },
  btnSeperator: {
    marginVertical: 10,
    marginHorizontal: width * 0.2,
    color: 'gray',
    opacity: 0.1,
  },
  loginTextParagraph: {
    fontWeight: '700',
    fontSize: 16,
    color: '#0288d1',
    // textDecorationLine: 'underline'
  },
  iconText: {
    marginLeft: -width * 0.15,
  },
  verticalSeperator: {
    borderRightWidth: 1,
    borderRightColor: '#e0e0e044',
  },
  logoShadow: {
    // marginBottom: -40,

    color: '#fff',
    textShadowOffset: {width: 1, height: 3},
    textShadowRadius: 10,
    textShadowColor: 'rgba(46,91,255,0.3)',
  },
});
