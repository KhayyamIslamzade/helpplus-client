import React, {Component} from 'react';
import {Image, StyleSheet} from 'react-native';
import {TabView, TabBar, SceneMap} from 'react-native-tab-view';
import {TimelineContent, GridContent} from './';
import {icons, colors, DIMENSIONS, tintColor} from '../../constants/theme';

export default class ProfileTabView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      index: 0,
      routes: [{key: 'timeline'}, {key: 'grid'}],
    };
  }
  renderScene = SceneMap({
    timeline: TimelineContent,
    grid: GridContent,
  });

  renderIcon = ({route, focused}) => {
    let tintC = focused ? tintColor.activeSecondary : tintColor.inactive;
    let image = null;
    switch (route.key) {
      case 'timeline':
        image = (
          <Image
            source={icons.timelinePhotoIcon}
            style={[styles.icon, {tintColor: tintC}]}
          />
        );
        break;
      case 'grid':
        image = (
          <Image
            source={icons.gridPhotoIcon}
            style={[styles.icon, {tintColor: tintC}]}
          />
        );
        break;
    }
    return image;
  };
  renderTabBar = props => {
    return (
      <TabBar
        scrollEnabled={true}
        {...props}
        renderIcon={this.renderIcon}
        indicatorStyle={{backgroundColor: colors.green}}
        style={styles.tabbar}
      />
    );
  };

  render() {
    return (
      <TabView
        navigationState={this.state}
        renderScene={this.renderScene}
        renderTabBar={this.renderTabBar}
        onIndexChange={index => this.setState({index})}
        initialLayout={{width: DIMENSIONS.WINDOW_WIDTH}}
      />
    );
  }
}

const styles = StyleSheet.create({
  icon: {
    width: 25,
    height: 25,
  },

  tabbar: {
    backgroundColor: '#FFF',
    overflow: 'hidden',
  },
});
