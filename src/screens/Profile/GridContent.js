import React, {Component} from 'react';
import {Text, Block} from '../../components/core';
import {StyleSheet} from 'react-native';
export default class GridContent extends Component {
  render() {
    return (
      <Block flex={1}>
        <Text> Content 1 </Text>
      </Block>
    );
  }
}

const styles = StyleSheet.create({});
