import React, {Component} from 'react';
import {Text, Block, List, PostItem, Button} from '../../components/core';
import {StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import {postActions} from '../../redux/actions';

import ActivitySpinner from '../../components/animations/ActivitySpinner';

class TimelineContent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      numColumns: 3,
    };
  }
  componentWillMount() {
    const {dispatch} = this.props;
    dispatch(postActions.getPosts());
  }

  render() {
    const {datas, datasFetching} = this.props;
    console.log('TCL: TimelineContent -> render -> datas', datas);

    return (
      <Block flex={1} style={styles.container}>
        {datasFetching && <ActivitySpinner />}

        <List
          data={datas}
          renderItem={({item}) => (
            <PostItem
              style={styles.itemStyle}
              numColumns={this.state.numColumns}
              data={item}
            />
          )}
          keyExtractor={item => item.id}
          numColumns={this.state.numColumns}
        />
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  itemStyle: {
    margin: 1,
  },
  container: {
    margin: 2,
  },
});

function mapStateToProps(state) {
  const {datasFetched, datasFetching, datas, post} = state.post;

  return {
    datasFetched,
    datasFetching,
    datas,
    post,
  };
}
export default connect(mapStateToProps)(TimelineContent);
