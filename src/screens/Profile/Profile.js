import React, {Component} from 'react';
import {
  Image,
  ImageBackground,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
} from 'react-native';
import {connect} from 'react-redux';
import {Block, Button, Text} from '../../components/core';
import {TabView} from './';
import {images} from '../../constants/contents';
import {icons, colors, DIMENSIONS} from '../../constants/theme';

const imageSize = DIMENSIONS.WINDOW_WIDTH / 4.5;

class Profile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user: props.user ? props.user : '_username_',
    };
  }

  settingIconClicked = () => {
    alert('clicked');
  };

  render() {
    return (
      <SafeAreaView style={styles.container} forceInset={{top: 'never'}}>
        <StatusBar backgroundColor="transparent" barStyle="dark-content" />

        <Block flex={1}>
          <Block center middle>
            <ImageBackground
              source={images.userImage}
              borderRadius={imageSize}
              width={imageSize}
              height={imageSize}
              style={styles.profileImg}>
              <Button
                style={styles.iconContainer}
                onPress={this.settingIconClicked}>
                <Image
                  source={icons.cameraIcon}
                  style={[styles.icon, {tintColor: colors.white}]}></Image>
              </Button>
            </ImageBackground>
          </Block>

          <Block flex="disabled">
            <Text caption="md" color="green" center>
              {this.state.user.userName}
            </Text>

            <Text
              pargraph="md"
              color="gray"
              center
              style={{marginHorizontal: 40}}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras
              commodo rutrum placerat.
            </Text>
          </Block>

          <Block
            flex={1}
            style={{
              marginTop: 20,
              borderTopColor: colors.gray2,
              borderTopWidth: 1,
            }}>
            <TabView />
          </Block>
        </Block>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  profileImg: {
    width: imageSize,
    height: imageSize,
  },
  iconContainer: {
    backgroundColor: colors.green,
    borderColor: colors.white,
    borderWidth: 2,
    position: 'absolute',
    borderRadius: 20,
    padding: 5,
    right: 0,
    top: 0,
  },
  icon: {
    width: 18,
    height: 18,
  },
});

function mapStateToProps(state) {
  const {user} = state.authentication;
  return {
    user,
  };
}

export default connect(mapStateToProps)(Profile);
