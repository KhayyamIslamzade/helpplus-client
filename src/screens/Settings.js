import React, {Component} from 'react';
import {Image} from 'react-native';
import {connect} from 'react-redux';
import {Button, Text, Block, Input} from '../components/core';
import {icons} from '../constants/theme';
import {userActions} from '../redux/actions';

class Settings extends Component {
  constructor(props) {
    super(props);

    this.onLogout = this.onLogout.bind(this);
  }

  onLogout() {
    const {dispatch} = this.props;
    dispatch(userActions.logout());
    this.props.navigation.navigate('Auth');
  }
  render() {
    return (
      <Block flex={1} style={{margin: 20}}>
        <Button
          backgroundColor="blue"
          borderRadius="lg"
          style={{
            marginBottom: 5,
            paddingVertical: 5,
          }}>
          <Block row>
            <Block flex={1} middle center>
              <Image
                source={icons.mailIcon}
                style={{
                  width: 30,
                  height: 30,
                  resizeMode: 'stretch',
                  tintColor: '#FFF',
                }}
              />
            </Block>
            <Block flex={4} middle>
              <Text button left>
                Account
              </Text>
            </Block>
          </Block>
        </Button>

        <Button
          backgroundColor="blue"
          borderRadius="lg"
          style={{
            marginBottom: 5,
            paddingVertical: 5,
          }}>
          <Block row>
            <Block flex={1} middle center>
              <Image
                source={icons.mailIcon}
                style={{
                  width: 30,
                  height: 30,
                  resizeMode: 'stretch',
                  tintColor: '#FFF',
                }}
              />
            </Block>
            <Block flex={4} middle>
              <Text button left>
                Privacy
              </Text>
            </Block>
          </Block>
        </Button>

        <Button
          backgroundColor="blue"
          borderRadius="lg"
          style={{
            marginBottom: 5,
            paddingVertical: 5,
          }}>
          <Block row>
            <Block flex={1} middle center>
              <Image
                source={icons.mailIcon}
                style={{
                  width: 30,
                  height: 30,
                  resizeMode: 'stretch',
                  tintColor: '#FFF',
                }}
              />
            </Block>
            <Block flex={4} middle>
              <Text button left>
                Help
              </Text>
            </Block>
          </Block>
        </Button>

        <Button
          backgroundColor="default"
          borderRadius="lg"
          style={{
            marginBottom: 5,
            paddingVertical: 5,
          }}>
          <Block row>
            <Block flex={1} middle center>
              <Image
                source={icons.mailIcon}
                style={{
                  width: 30,
                  height: 30,
                  resizeMode: 'stretch',
                  tintColor: '#FFF',
                }}
              />
            </Block>
            <Block flex={4} middle>
              <Text button left>
                About
              </Text>
            </Block>
          </Block>
        </Button>

        <Block flex={1} right>
          <Button
            backgroundColor="green"
            borderRadius="lg"
            style={{
              paddingVertical: 5,
            }}
            onPress={this.onLogout}>
            <Text button center h4>
              Logout
            </Text>
          </Button>
        </Block>
      </Block>
    );
  }
}

export default connect()(Settings);
