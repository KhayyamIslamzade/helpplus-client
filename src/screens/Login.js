import React, {Component} from 'react';
import {
  ImageBackground,
  SafeAreaView,
  Dimensions,
  StyleSheet,
  KeyboardAvoidingView,
} from 'react-native';
import {connect} from 'react-redux';
import {userActions} from '../redux/actions';
import {Button, Text, Block, Input} from '../components/core';
import {icons} from '../constants/theme';
import deviceStorage from '../services/deviceStorage.service';

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user: {
        emailOrUsername: 'khayyam',
        password: 'Aa12345!',
      },
    };

    this.onSignIn = this.onSignIn.bind(this);
  }
  onSignIn = () => {
    if (this.state.user.emailOrUsername && this.state.user.password) {
      const {dispatch} = this.props;
      const {emailOrUsername, password} = this.state.user;
      dispatch(userActions.login(emailOrUsername, password));
    }
  };
  componentWillReceiveProps(props) {
    if (props.loggedIn) {
      this.props.navigation.navigate('Profile');
    }
  }
  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        <KeyboardAvoidingView style={{flex: 1}} behavior="padding">
          <Block flex={1}>
            <ImageBackground
              accessibilityRole={'image'}
              resizeMethod={'auto'}
              source={require('../assets/images/background-v4.png')}
              style={styles.background}
              imageStyle={styles.backgroundImage}></ImageBackground>
          </Block>
          <Block flex={2} right center>
            <Text caption="logo" color="green" style={{marginBottom: -40}}>
              h+
            </Text>
          </Block>

          <Block flex={4} style={{marginHorizontal: 10}}>
            <Block flex={2} center middle style={{marginVertical: 5}}>
              <Input
                primaryFont
                h4
                full
                email
                placeholder="E-mail / Username"
                placeholderTextColor="#A6A6A6"
                iconSource={icons.userIcon}
                style={{margin: 10}}
                value={this.state.user.emailOrUsername}
                onChangeText={text => {
                  this.setState({
                    user: {
                      ...this.state.user,
                      emailOrUsername: text,
                    },
                  });
                }}
              />

              <Input
                primaryFont
                h4
                full
                password
                placeholder="Password"
                placeholderTextColor="#A6A6A6"
                iconSource={icons.passwordIcon}
                style={{margin: 10}}
                value={this.state.user.password}
                onChangeText={text => {
                  this.setState({
                    user: {
                      ...this.state.user,
                      password: text,
                    },
                  });
                }}
              />
            </Block>
            <Block flex={1} center middle style={{marginVertical: 10}}>
              <Button
                full
                backgroundColor="green"
                borderRadius="lg"
                shadow="low"
                style={{
                  height: 55,
                  marginVertical: 5,
                  paddingVertical: 15,
                }}
                onPress={this.onSignIn}>
                <Text button center h3>
                  Login
                </Text>
              </Button>
            </Block>
          </Block>
        </KeyboardAvoidingView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  background: {
    width: '100%',
    height: '100%',
  },
  backgroundImage: {
    resizeMode: 'stretch',
    alignSelf: 'flex-start',
  },
});
//store data
function mapStateToProps(state) {
  const {loggingIn, loggedIn} = state.authentication;
  return {
    loggingIn,
    loggedIn,
  };
}

export default connect(mapStateToProps)(Login);
