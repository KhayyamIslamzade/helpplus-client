import React, {Component} from 'react';
import {SafeAreaView, StatusBar, StyleSheet} from 'react-native';

import {connect} from 'react-redux';
import {Block} from '../components/core';
import LogoFadeBounce from '../components/animations/LogoFadeBounce';
import * as theme from '../constants/theme';

class AuthLoading extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        <StatusBar translucent backgroundColor="transparent" />
        <Block flex={1} center middle style={styles.container}>
          <LogoFadeBounce navigation={this.props.navigation} />
        </Block>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: theme.colors.green,
  },
});

export default connect()(AuthLoading);
