import Dashboard from './Dashboard';
import Explore from './Explore';
import Profile from './Profile/Profile';
import Activity from './Activity';
import Settings from './Settings';
import Main from './Main';
import Login from './Login';
import Register from './Register';
import AuthLoading from './AuthLoading';
import NewPost from './NewPost';
import AnimateTest from './AnimateTest';
import SpinnerScratch from './SpinnerScratch';

export {
  Settings,
  Dashboard,
  Explore,
  Activity,
  Profile,
  Main,
  Login,
  Register,
  AuthLoading,
  NewPost,
  AnimateTest,
  SpinnerScratch,
};
