import React, {Component} from 'react';
import {Image, StyleSheet, Dimensions} from 'react-native';
import {Block, Button, Input, Text} from '../components/core';
import {connect} from 'react-redux';
import {HeaderBackButton} from 'react-navigation-stack';
import {DIMENSIONS} from '../constants/theme';
import {postActions} from '../redux/actions';
class NewPost extends Component {
  constructor(props) {
    super(props);
    this.state = {
      image: props.image,
      caption: '',
    };

    this.shareOnClick = this.shareOnClick.bind(this);
  }

  static navigationOptions = ({navigation}) => {
    return {
      title: 'New Post',
      headerTitleStyle: styles.header,
      headerLeft: (
        <HeaderBackButton
          onPress={() => {
            navigation.goBack(null);
          }}
        />
      ),
      headerRight: (
        <Button
          style={styles.headerRightBtn}
          onPress={navigation.getParam('shareOnClick')}>
          <Text color="blue" paragraph="md" bold>
            Share
          </Text>
        </Button>
      ),
    };
  };

  componentDidMount() {
    this.props.navigation.setParams({
      shareOnClick: this.shareOnClick,
    });
  }
  shareOnClick() {
    const {dispatch, navigation} = this.props;
    const {image, caption} = this.state;
    dispatch(
      postActions.add({
        image: {
          uri: image.uri,
          name: image.modificationDate,
          type: image.mime,
        },
        caption,
      }),
    );
    navigation.goBack(null);
  }
  renderImage(image) {
    return (
      <Image
        style={{
          width: DIMENSIONS.WINDOW_WIDTH / 5,
          height: DIMENSIONS.WINDOW_WIDTH / 5,
          resizeMode: 'contain',
        }}
        source={image}
      />
    );
  }

  render() {
    return (
      <Block flex={1} style={styles.container}>
        <Block row flex={1}>
          <Block flex={1}>
            {this.state.image ? this.renderImage(this.state.image) : null}
          </Block>
          <Block flex={4}>
            <Input
              full
              multiline={true}
              placeholder="Write a caption..."
              placeholderTextColor="#A6A6A6"
              style={styles.input}
              onChangeText={text => {
                this.setState({
                  ...this.state,
                  caption: text,
                });
              }}
            />
          </Block>
        </Block>
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginVertical: 20,
    marginHorizontal: 10,
  },
  header: {
    textAlign: 'center',
    flexGrow: 1,
    alignSelf: 'center',
  },
  headerRightBtn: {
    marginRight: 20,
  },
  headerRightText: {
    fontWeight: 'bold',
  },
  input: {
    marginHorizontal: 15,
    height: DIMENSIONS.WINDOW_WIDTH / 5,
    maxHeight: DIMENSIONS.WINDOW_WIDTH / 5,
  },
});

function mapStateToProps(state) {
  const {image} = state.post;

  return {
    image,
  };
}
export default connect(mapStateToProps)(NewPost);
