import React, {Component} from 'react';
import {
  Text,
  View,
  Image,
  ImageBackground,
  TouchableOpacity,
  Dimensions,
  StatusBar,
  StyleSheet,
} from 'react-native';

export default class ActivityScreen extends Component {
  render() {
    return (
      <View style={{flex: 1, opacity: 0.2, backgroundColor: 'gray'}}>
        <Text>Activity</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({});
