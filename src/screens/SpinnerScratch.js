import React, {useState, useEffect, Component} from 'react';
import {
  SafeAreaView,
  StatusBar,
  AppRegistry,
  StyleSheet,
  ActivityIndicator,
  Easing,
  Animated,
  TouchableHighlight,
  ScrollView,
} from 'react-native';

import {Block, Text, Button} from '../components/core';
import * as theme from '../constants/theme';
export default class SpinnerScratch extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        <StatusBar translucent backgroundColor="transparent" />
        <Block flex={1} center middle style={styles.container}>
          {/* <LogoFadeInView /> */}
          <ActivityIndicator
            size="large"
            color="green"
            style={{
              position: 'absolute',
              backgroundColor: '#22222222',
              left: 0,
              right: 0,
              bottom: 0,
              top: 0,
            }}
          />
        </Block>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: theme.colors.green,
  },
  block: {
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: '#22222266',

    backgroundColor: theme.colors.white,

    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  text: {
    fontFamily: theme.primaryFontFamily,
    color: theme.colors.green,
  },
});
