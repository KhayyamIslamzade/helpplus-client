import React, {Component} from 'react';
import {
  ImageBackground,
  SafeAreaView,
  Dimensions,
  StyleSheet,
  KeyboardAvoidingView,
} from 'react-native';
import {connect} from 'react-redux';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import validationWrapper from '../utils/validation.wrapper';
import {registerRules} from '../constants/validation.rules';
import {Button, Text, Block, Input} from '../components/core';
import {userActions} from '../redux/actions';
import {icons} from '../constants/theme';

const {width} = Dimensions.get('window');

class Register extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user: {
        userName: '',
        email: '',
        password: '',
        confirmPassword: '',
      },
      errors: {
        userNameError: '',
        emailError: '',
        passwordError: '',
        confirmPasswordError: '',
      },
    };
    this.vp = new validationWrapper(registerRules);
    this.onSignUp = this.onSignUp.bind(this);
  }
  onSignUp = () => {
    const emailError = this.vp.validate('email', this.state.user.email);
    const userNameError = this.vp.validate(
      'userName',
      this.state.user.userName,
    );
    const passwordError = this.vp.validate(
      'password',
      this.state.user.password,
    );
    const confirmPasswordError = this.vp.validate('confirmPassword', {
      password: this.state.user.password,
      confirmPassword: this.state.user.confirmPassword,
    });
    console.log(confirmPasswordError);
    this.setState({
      errors: {
        ...this.state.errors,
        userNameError: userNameError,
        emailError: emailError,
        passwordError: passwordError,
        confirmPasswordError: confirmPasswordError,
      },
    });

    if (
      !(emailError || userNameError || passwordError || confirmPasswordError)
    ) {
      const {user} = this.state;
      const {dispatch} = this.props;
      dispatch(userActions.register(user));
    }
  };

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        <KeyboardAwareScrollView
          contentContainerStyle={{flexGrow: 1}}
          enableOnAndroid={true}>
          <Block flex={1}>
            <ImageBackground
              accessibilityRole={'image'}
              resizeMethod={'auto'}
              source={require('../assets/images/background-v4.png')}
              style={styles.background}
              imageStyle={styles.backgroundImage}></ImageBackground>
          </Block>
          <Block flex={1} center middle>
            <Text caption="logo" color="green" style={{marginBottom: -40}}>
              h+
            </Text>
          </Block>

          <Block flex={5} style={{marginHorizontal: 10}}>
            <Block
              flex={1}
              right
              style={{marginBottom: 5, paddingLeft: width * 0.05}}>
              <Text caption="lg" color="green" style={{marginBottom: -40}}>
                Registration
              </Text>
            </Block>
            <Block flex={4} center middle style={{marginBottom: 5}}>
              <Input
                primaryFont
                h4
                full
                email
                placeholder="E-mail"
                placeholderTextColor="#A6A6A6"
                style={{margin: 10}}
                iconSource={icons.mailIcon}
                errorIconSource={icons.errorIcon}
                onChangeText={text => {
                  this.setState({
                    user: {
                      ...this.state.user,
                      email: text,
                    },
                    errors: {
                      ...this.state.errors,
                      emailError: this.vp.validate('email', text),
                    },
                  });
                }}
                error={this.state.errors.emailError}
              />
              <Input
                primaryFont
                h4
                full
                placeholder="Username"
                placeholderTextColor="#A6A6A6"
                style={{margin: 10}}
                iconSource={icons.userIcon}
                errorIconSource={icons.errorIcon}
                onChangeText={text => {
                  this.setState({
                    user: {
                      ...this.state.user,
                      userName: text,
                    },
                    errors: {
                      ...this.state.errors,
                      userNameError: this.vp.validate('userName', text),
                    },
                  });
                }}
                error={this.state.errors.userNameError}
              />

              <Input
                primaryFont
                h4
                full
                password
                placeholder="Password"
                placeholderTextColor="#A6A6A6"
                style={{margin: 10}}
                iconSource={icons.passwordIcon}
                errorIconSource={icons.errorIcon}
                onChangeText={text => {
                  this.setState({
                    user: {
                      ...this.state.user,
                      password: text,
                    },
                    errors: {
                      ...this.state.errors,
                      passwordError: this.vp.validate('password', text),
                    },
                  });
                }}
                error={this.state.errors.passwordError}
              />
              <Input
                primaryFont
                h4
                full
                password
                placeholder="Confirm Password"
                placeholderTextColor="#A6A6A6"
                style={{margin: 10}}
                iconSource={icons.passwordIcon}
                errorIconSource={icons.errorIcon}
                onChangeText={text => {
                  this.setState({
                    user: {
                      ...this.state.user,
                      confirmPassword: text,
                    },
                    errors: {
                      ...this.state.errors,
                      confirmPasswordError: this.vp.validate(
                        'confirmPassword',
                        {
                          password: this.state.user.password,
                          confirmPassword: text,
                        },
                      ),
                    },
                  });
                }}
                error={this.state.errors.confirmPasswordError}
              />
            </Block>

            <Block flex={2} center middle style={{marginBottom: 10}}>
              <Button
                full
                backgroundColor="green"
                borderRadius="lg"
                shadow="low"
                style={{
                  height: 55,
                  marginVertical: 5,
                  paddingVertical: 15,
                }}
                onPress={this.onSignUp}>
                <Text button center h3>
                  Sign Up
                </Text>
              </Button>
              <Text caption="sm" style={{color: '#222'}}>
                By continuing , you agree to Help+
                <Text style={{textDecorationLine: 'underline'}}>
                  {' '}
                  Terms of Use {'\n'} and Privacy Policy{' '}
                </Text>
              </Text>
            </Block>
          </Block>
        </KeyboardAwareScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  background: {
    width: '100%',
    height: '100%',
  },
  backgroundImage: {
    resizeMode: 'stretch',
    alignSelf: 'flex-start',
  },
});

//store data
const mapStateToProps = state => {
  const {registering} = state.registration;
  return {
    registering,
  };
};
export default connect(mapStateToProps)(Register);
