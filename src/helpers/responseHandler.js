const handleResponse = response => {
  console.log('TCL: response', response);
  return new Promise((resolve, reject) => {
    if (response.status === 200) {
      resolve(response);
    } else {
      reject(response.error);
    }
  }).catch(error => {
    console.log(error);
  });
};

const handleError = error => {
  return Promise.reject(error && error.message);
};

export {handleResponse, handleError};
