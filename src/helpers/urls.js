const apiUrl = 'https://dfe7c8fa.ngrok.io';
// const apiUrl = 'https://helpplusserver.azurewebsites.net';

const AUTHORIZATION_URLS = {
  LOGIN_URL: apiUrl + '/api/account/login',
  REGISTER_URL: apiUrl + '/api/account/register',
};

const POST_URLS = {
  ADD_URL: apiUrl + '/api/post/add',
  FETCH_URL: apiUrl + '/api/post/getall',
};

export {AUTHORIZATION_URLS, POST_URLS};
