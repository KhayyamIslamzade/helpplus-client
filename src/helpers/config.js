import axios from 'react-native-axios';
import {AlertBox} from '../components/core/Alert';
import {deviceStorage} from '../services/deviceStorage.service';
import {getToken} from '../utils/auth';
const options = {
  headers: {
    Authorization: null,
    'Content-Type': 'application/json',
  },
};
let axiosInstance = axios.create(options);
axiosInstance.interceptors.request.use(
  async request => {
    await getToken().then(token => {
      request.headers['Authorization'] = 'Bearer ' + token;
    });
    console.log('TCL: response', request);
    return Promise.resolve(request);
  },

  error => {
    // do something with request error
    console.log(error); // for debug
    return;
  },
);

// request interceptor

// response interceptor
axiosInstance.interceptors.response.use(
  /**
   * If you want to get http information such as headers or status
   * Please return  response => response
   */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code
   */
  response => {
    const res = response.data;
    console.log('TCL: response', response);
    console.log('TCL: res', res);
    // console.log('TCL: res', res);

    // if the custom code is not 20000, it is judged as an error.
    if (res.code && res.code !== 20000) {
      // Message({
      //   message: res.message || 'Error',
      //   type: 'error',
      //   duration: 5 * 1000,
      // });

      // // 50008: Illegal token; 50012: Other clients logged in; 50014: Token expired;
      // if (res.code === 50008 || res.code === 50012 || res.code === 50014) {
      //   // to re-login
      //   MessageBox.confirm(
      //     'You have been logged out, you can cancel to stay on this page, or log in again',
      //     'Confirm logout',
      //     {
      //       confirmButtonText: 'Re-Login',
      //       cancelButtonText: 'Cancel',
      //       type: 'warning',
      //     },
      //   ).then(() => {
      //     store.dispatch('user/resetToken').then(() => {
      //       location.reload();
      //     });
      //   });
      // }
      console.log('TCL: res', res);
      return Promise.reject(new Error(res.message || 'Error'));
    } else {
      console.log('TCL: res', res);
      return Promise.resolve(res);
    }
  },
  error => {
    if (error.response.data) {
      AlertBox(error.response.data);
    } else {
      AlertBox(error);
    }

    return Promise.reject(error);
  },
);
export {axiosInstance as axios};
