import {postActionTypes} from '../actionTypes/index';
import {postService} from '../../services';
// import { alertActions } from './';
// import { history } from '../_helpers';
// let post = {
//   request: post => {
//     return {type: postActionTypes.POST_ADD_REQUEST, post};
//   },
//   success: post => {
//     return {type: postActionTypes.POST_ADD_SUCCESS, post};
//   },
//   failure: error => {
//     return {type: postActionTypes.POST_ADD_FAILURE, error};
//   },
// };

// let image = {
//   success: image => {
//     return {type: postActionTypes.IMAGE_SELECT_SUCCESS, image};
//   },
//   failure: error => {
//     return {type: postActionTypes.IMAGE_SELECT_FAILURE, error};
//   },
// };

add = data => {
  return dispatch => {
    dispatch(request());

    postService.add(data).then(
      response => {
        console.log('TCL: response', response);
        dispatch(success(response));
      },
      error => {
        console.log(error);
        dispatch(failure(error));
      },
    );
  };

  function request() {
    return {type: postActionTypes.POST_ADD_REQUEST};
  }
  function success(data) {
    return {type: postActionTypes.POST_ADD_SUCCESS, post: data};
  }
  function failure(error) {
    return {type: postActionTypes.POST_ADD_FAILURE, error};
  }
};

getPosts = () => {
  return dispatch => {
    dispatch(request());
    postService.getPosts().then(
      response => {
        console.log('TCL: getPosts -> response', response);
        dispatch(success(response));
      },
      error => {
        dispatch(failure(error));
      },
    );
  };

  function request() {
    return {type: postActionTypes.POST_FETCH_REQUEST};
  }
  function success(datas) {
    return {type: postActionTypes.POST_FETCH_SUCCESS, datas};
  }
  function failure(error) {
    return {type: postActionTypes.POST_FETCH_FAILURE, error};
  }
};

imageSelect = imageSelectPromise => {
  return dispatch => {
    imageSelectPromise.then(
      response => {
        const image = {
          uri: response.path,
          width: response.width,
          height: response.height,
          mime: response.mime,
          modificationDate: response.modificationDate,
        };
        dispatch(success(image));
      },
      error => {
        dispatch(failure(error));
      },
    );
  };

  function success(data) {
    return {type: postActionTypes.IMAGE_SELECT_SUCCESS, image: data};
  }
  function failure(error) {
    return {type: postActionTypes.IMAGE_SELECT_FAILURE, error};
  }
};

export const postActions = {
  add,
  getPosts,
  imageSelect,
};
