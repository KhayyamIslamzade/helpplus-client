import {userActionTypes} from '../actionTypes/index';
import {userService} from '../../services';
import {deviceStorage} from '../../services/deviceStorage.service';

function login(emailOrUsername, password) {
  return dispatch => {
    dispatch(request({emailOrUsername}));

    userService.login(emailOrUsername, password).then(
      response => {
        if (response.token) {
          deviceStorage.saveItem('token', response.token);
          deviceStorage.saveItem('user', JSON.stringify(response.user));
        }
        dispatch(success(response));
      },
      error => {
        dispatch(failure(error));
      },
    );
  };

  function request(user) {
    return {type: userActionTypes.LOGIN_REQUEST, user};
  }
  function success(data) {
    return {type: userActionTypes.LOGIN_SUCCESS, data};
  }
  function failure(error) {
    return {type: userActionTypes.LOGIN_FAILURE, error};
  }
}

function register(user) {
  return dispatch => {
    dispatch(request(user));

    userService.register(user).then(
      response => {
        dispatch(success(user));
      },
      error => {
        dispatch(failure(error));
      },
    );
  };

  function request(user) {
    return {type: userActionTypes.REGISTER_REQUEST, user};
  }
  function success(user) {
    return {type: userActionTypes.REGISTER_SUCCESS, user};
  }
  function failure(error) {
    return {type: userActionTypes.REGISTER_FAILURE, error};
  }
}
function logout() {
  //   userService.logout();
  deviceStorage.multiRemove(['token', 'user']);
  return {type: userActionTypes.LOGOUT};
}

export const userActions = {
  register,
  logout,
  login,
};
