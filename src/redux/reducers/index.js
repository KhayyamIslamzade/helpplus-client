import { combineReducers } from 'redux';
import registration from './registration.reducer';
import authentication from './authentication.reducer';
import post from './post.reducer';

const allReducers = combineReducers({
    registration,
    authentication,
    post
});
export default allReducers;