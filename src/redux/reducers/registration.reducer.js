
import { userActionTypes } from '../actionTypes';

export default function registration(state = {}, action) {

    switch (action.type) {
        case userActionTypes.REGISTER_REQUEST:
            console.log("REGISTER_REQUEST");
            return { registering: true };
        case userActionTypes.REGISTER_SUCCESS:
            console.log("REGISTER_SUCCESS");
            return {};
        case userActionTypes.REGISTER_FAILURE:
            console.log("REGISTER_FAILURE");
            return {};
        default:
            return state
    }
}