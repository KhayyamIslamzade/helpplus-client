import {postActionTypes} from '../actionTypes';

export default function post(state = {}, action) {
  switch (action.type) {
    case postActionTypes.IMAGE_SELECT_REQUEST:
      return {
        ...state,
        imageSelecting: true,
        imageSelected: false,
      };
    case postActionTypes.IMAGE_SELECT_SUCCESS:
      return {
        ...state,
        imageSelecting: false,
        imageSelected: true,
        image: action.image,
      };
    case postActionTypes.IMAGE_SELECT_FAILURE:
      return {
        ...state,
        imageSelecting: false,
        imageSelected: false,
        image: null,
      };

    case postActionTypes.POST_ADD_REQUEST:
      return {
        ...state,
        postAdding: true,
      };
    case postActionTypes.POST_ADD_SUCCESS:
      console.log('TCL: post -> action', state);
      return {
        ...state,
        postAdded: true,
        postAdding: false,
        post: action.post,
        datas: [action.post, ...state.datas],
      };
    case postActionTypes.POST_ADD_FAILURE:
      return {
        ...state,
        postAdded: false,
        postAdding: false,
        post: null,
      };

    case postActionTypes.POST_FETCH_REQUEST:
      return {
        ...state,
        datasFetching: true,
      };
    case postActionTypes.POST_FETCH_SUCCESS:
      return {
        ...state,
        datasFetched: true,
        datasFetching: false,
        datas: action.datas,
      };
    case postActionTypes.POST_FETCH_FAILURE:
      return {
        ...state,
        datasFetched: false,
        datasFetching: false,
        datas: null,
      };

    default:
      return state;
  }
}
