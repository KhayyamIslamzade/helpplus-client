
import { userActionTypes } from '../actionTypes';
import deviceStorage from '../../services/deviceStorage.service';
let user, token;
// deviceStorage.setItem('token', token);
// token = await deviceStorage.getItem('token');

// await deviceStorage.getItem('user').then(
//     (value) => {
//         if (value !== null) {
//             user = JSON.parse(value);
//         }
//     }
// );

// let user = JSON.parse(deviceStorage.getItem('user'));
// // let token = deviceStorage.getItem('token');
// const initialState = user && token ? { loggedIn: true, user, token } : {};

// console.log(token);
// console.log(initialState);
const initialState = {
    token: "",
    user: "",
};
// let user = JSON.parse(localStorage.getItem('user'));
// const initialState = user ? { loggedIn: true, user } : {};
export default function authentication(state = initialState, action) {

    switch (action.type) {
        case userActionTypes.LOGIN_REQUEST:

            return {
                ...state,
                loggingIn: true,
                user: action.user
            };
        case userActionTypes.LOGIN_SUCCESS:
            return {
                ...state,
                loggedIn: true,
                loggingIn: false,
                user: action.data.user,
                token: action.data.token
            };
        case userActionTypes.LOGIN_FAILURE:

            return {
                ...state,
                loggingIn: false,
                user: null,
                token: null
            };
        case userActionTypes.LOGOUT:
            console.log("logout", action);
            return {
                ...state,
                loggedIn: false,
                user: null,
                token: null
            };
        default:
            return state
    }
}