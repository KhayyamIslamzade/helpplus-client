import { userActionTypes } from './user.actionTypes'
import { postActionTypes } from './post.actionTypes'

export {
    userActionTypes,
    postActionTypes
}