import React, {Component} from 'react';
import {View, Image, TouchableOpacity, StyleSheet} from 'react-native';
import {withNavigation} from 'react-navigation';
class SettingsButton extends Component {
  constructor(props) {
    super(props);
    this.handleOpenDrawer = this.handleOpenDrawer.bind(this);
  }

  handleOpenDrawer = () => {
    this.props.navigation.state.isDrawerOpen
      ? this.props.navigation.closeDrawer()
      : this.props.navigation.openDrawer();
  };
  render() {
    return (
      <TouchableOpacity
        style={{
          width: 44,
          height: 44,
          marginLeft: 20,
          alignItems: 'center',
          justifyContent: 'center',
        }}
        onPress={this.handleOpenDrawer}>
        <Image
          source={this.props.iconPath}
          style={{
            width: 25,
            height: 25,
            tintColor: '#222',
          }}
        />
      </TouchableOpacity>
    );
  }
}
export default withNavigation(SettingsButton);
