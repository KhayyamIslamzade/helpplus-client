import React, {Component} from 'react';
import {ActivityIndicator} from 'react-native';

export default class ActivitySpinner extends Component {
  render() {
    return (
      <ActivityIndicator
        size="large"
        color="green"
        style={{
          position: 'absolute',
          backgroundColor: '#22222222',
          left: 0,
          right: 0,
          bottom: 0,
          top: 0,
        }}
      />
    );
  }
}
