import React, {Component} from 'react';
import {StyleSheet, Animated, Easing} from 'react-native';
import {connect} from 'react-redux';
import {deviceStorage} from '../../services';
import {userActionTypes} from '../../redux/actionTypes/';
import * as theme from '../../constants/theme';

class LogoFadeBounce extends Component {
  constructor(props) {
    super(props);
    this.state = {
      bounceValue: new Animated.Value(0),
      rotateValue: new Animated.Value(0),
      opacity: new Animated.Value(0),
      decideEnded: false,
      navigateScreenName: '',
    };

    this.animate = this.animate.bind(this);
    this.plusAnimation = this.plusAnimation.bind(this);
    this.decideAsync = this.decideAsync.bind(this);
    this.canNavigateScreen = this.canNavigateScreen.bind(this);
  }

  componentDidMount() {
    this.decideAsync();
    this.animate();
  }
  decideAsync = async () => {
    const token = await deviceStorage.getItem('token');
    const user = await deviceStorage.getItem('user');
    let navigateScreenName = '';
    if (token && user) {
      const {dispatch} = this.props;

      dispatch({
        type: userActionTypes.LOGIN_SUCCESS,
        data: {
          token: token,
          user: JSON.parse(user),
        },
      });
      navigateScreenName = 'Profile';
    } else navigateScreenName = 'Auth';

    this.setState({
      ...this.state,
      decideEnded: true,
      navigateScreenName: navigateScreenName,
    });
  };

  createAnimation(value, duration, easing, delay = 0) {
    return Animated.timing(value, {
      toValue: 1,
      duration,
      easing,
      delay,
    });
  }
  plusAnimation() {
    this.state.rotateValue.setValue(0);
    if (this.canNavigateScreen()) {
      return null;
    } else {
      return this.createAnimation(
        this.state.rotateValue,
        1500,
        Easing.linear,
      ).start(this.plusAnimation);
    }
  }

  canNavigateScreen() {
    if (this.state.decideEnded) {
      this.props.navigation.navigate(this.state.navigateScreenName);
      return true;
    } else return false;
  }
  animate() {
    Animated.parallel([
      this.createAnimation(this.state.bounceValue, 1000, Easing.bounce),
      this.createAnimation(this.state.opacity, 1000, Easing.elastic(2)),
      Animated.timing(this.state.rotateValue, {
        toValue: 1,
        duration: 1500,
        delay: 500,
        easing: Easing.linear,
      }).start(this.plusAnimation),
    ]).start();
  }

  render() {
    const opacity = this.state.opacity;
    const size = this.state.bounceValue.interpolate({
      inputRange: [0, 1],
      outputRange: [50, 130],
    });
    const padding = this.state.bounceValue.interpolate({
      inputRange: [0, 1],
      outputRange: [0, 20],
    });
    const borderRadius = this.state.bounceValue.interpolate({
      inputRange: [0, 1],
      outputRange: [10, 40],
    });
    const elevation = this.state.bounceValue.interpolate({
      inputRange: [0, 0.5, 1],
      outputRange: [0, 32, 16],
    });
    const fontSize = this.state.bounceValue.interpolate({
      inputRange: [0, 1],
      outputRange: [
        theme.DIMENSIONS.WINDOW_WIDTH / 35,
        theme.DIMENSIONS.WINDOW_WIDTH / 6,
      ],
    });
    const marginBottom = this.state.bounceValue.interpolate({
      inputRange: [0, 1],
      outputRange: [(-1 * theme.DIMENSIONS.WINDOW_HEIGHT) / 2, 0],
    });
    const rotateText = this.state.rotateValue.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '360deg'],
    });
    return (
      <Animated.View
        style={[
          styles.block,
          {
            marginBottom,
            opacity: opacity,
            paddingHorizontal: padding,
            borderRadius: borderRadius,
            elevation: elevation,
          },
        ]}>
        <Animated.Text
          style={[
            styles.text,
            {
              fontSize: fontSize,
            },
          ]}>
          h
        </Animated.Text>
        <Animated.Text
          style={[
            styles.text,
            {
              fontSize: fontSize,
              // transform: [{rotate: rotateText}],
            },
          ]}>
          +
        </Animated.Text>
      </Animated.View>
    );
  }
}

const styles = StyleSheet.create({
  block: {
    flexDirection: 'row',
    // borderWidth: 1,
    // borderColor: '#22222266',

    backgroundColor: theme.colors.white,

    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  text: {
    fontFamily: theme.primaryFontFamily,
    color: theme.colors.green,
  },
});
export default connect()(LogoFadeBounce);
