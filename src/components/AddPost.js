import React, {Component} from 'react';
import {Image} from 'react-native';
// import ImagePicker from 'react-native-image-picker';
import {Block, Button} from './core';
import {icons} from '../constants/theme';
import {connect} from 'react-redux';
import {postActions} from '../redux/actions';
class AddPost extends Component {
  constructor(props) {
    super(props);
    this.state = {
      photo: null,
    };

    this.handleChoosePhoto = this.handleChoosePhoto.bind(this);
  }
  componentDidMount() {
    // console.log(this.props);
  }
  handleChoosePhoto = () => {
    const options = {
      noData: true,
    };

    //  this.props.navigation
    // this.props.navigation.navigate('NewPost');
    console.log('TCL: AddPost -> handleChoosePhoto -> this.props', this.props);
    // console.log(this.props);
    // console.log('TCL: AddPost -> handleChoosePhoto -> this.props', this.props);

    // ImagePicker.launchImageLibrary(options, response => {
    //     if (response.uri) {
    //         this.setState({ photo: response })
    //         const { dispatch } = this.props;
    //         dispatch(postActions.add({
    //             uri: response.uri,
    //             name: response.fileName,
    //             type: response.type
    //         }));
    //     }
    // })
  };
  render() {
    return (
      <Block
        center
        style={{
          position: 'absolute',
          bottom: 5,
        }}>
        <Button
          backgroundColor="green"
          borderRadius="xlg"
          style={{padding: 15}}
          underlayColor="white"
          onPress={this.handleChoosePhoto}>
          <Image
            name="plus"
            color="#F8F8F8"
            source={icons.addIcon}
            style={{
              width: 28,
              height: 28,
            }}
          />
        </Button>
      </Block>
    );
  }
}

export default connect()(AddPost);
