import React, {Component} from 'react';
import {Image, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import {icons, iconSizes, colors, tintColor} from '../constants/theme';
import {Block, Button, Text} from './core';
import {postActions} from '../redux/actions/';

import ImagePicker from 'react-native-image-crop-picker';
class TabBar extends Component {
  constructor(props) {
    super(props);

    this.pickSingle = this.pickSingle.bind(this);
  }

  pickSingle(cropit, circular = false, mediaType) {
    let imageSelectPromise = ImagePicker.openPicker({
      width: 500,
      height: 500,
      cropping: cropit,
      cropperCircleOverlay: circular,
      compressImageMaxWidth: 1000,
      compressImageMaxHeight: 1000,
      compressImageQuality: 1,
      compressVideoPreset: 'MediumQuality',
      includeExif: true,
    });

    const {dispatch, navigation} = this.props;
    dispatch(postActions.imageSelect(imageSelectPromise));

    imageSelectPromise
      .then(response => {
        console.log('TCL: TabBar -> pickSingle -> response', response);
        navigation.navigate('NewPost');
      })
      .catch(error => {
        navigation.goBack(null);
      });
  }

  renderItem = (route, index) => {
    const {navigation, jumpTo} = this.props;
    const isNewPost = route.routeName === 'AddPost';
    const focused = index === navigation.state.index;

    return (
      <Button
        type="WithoutFeedback"
        key={route.key}
        style={styles.tab}
        onPress={() =>
          isNewPost ? this.pickSingle(true) : jumpTo(route.routeName)
        }>
        <Block flex={1} center middle>
          {this.renderIcon(route.routeName, focused)}
        </Block>
      </Button>
    );
  };
  renderIcon(routeName, focused) {
    let tintC = focused ? tintColor.active : tintColor.inactive;

    let source = '';
    switch (routeName) {
      case 'Profile':
        source = icons.profileIcon;
        break;
      case 'Activity':
        source = icons.activityIcon;
        break;
      case 'AddPost':
        source = icons.addIcon;
        tintC = tintColor.active;
        break;
      case 'Explore':
        source = icons.exploreIcon;
        break;
      case 'Dashboard':
        source = icons.dashboardIcon;
        break;
      default:
        source = icons.profileIcon;
        break;
    }
    return (
      <Image source={source} style={[styles.imageButton, {tintColor: tintC}]} />
    );
  }
  render() {
    const {routes} = this.props.navigation.state;

    return (
      <Block row style={styles.tabBar}>
        {routes && routes.map(this.renderItem)}
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  tabBar: {
    height: 49,
    borderTopWidth: StyleSheet.hairlineWidth,
    borderTopColor: 'rgba(0, 0, 0, .3)',
    // backgroundColor: '#F7F7F7',
    backgroundColor: colors.green,
  },
  imageButton: {
    resizeMode: 'stretch',
    width: iconSizes.width,
    height: iconSizes.height,
  },
});

export default connect()(TabBar);
