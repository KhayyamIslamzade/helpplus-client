import React, {Component} from 'react';
import {FlatList} from 'react-native';

// import Footer from './Footer';

export default class List extends Component {
  constructor(props) {
    super(props);
  }
  //   renderItem = ({item}) => <Item {...item} />;
  //   keyExtractor = item => item.id;
  render() {
    // const {onPressFooter, ...props} = this.props;

    const {...props} = this.props;
    return (
      <FlatList
        // scrollEnabled={false}
        // keyExtractor={this.keyExtractor}
        // ListFooterComponent={footerProps => (
        //   <Footer {...footerProps} onPress={onPressFooter} />
        // )}
        // renderItem={this.renderItem}
        {...props}
      />
    );
  }
}
