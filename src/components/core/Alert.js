import React, {Component} from 'react';
import {Alert} from 'react-native';

const AlertBox = msg => {
  //   console.log('TCL: msg', msg);
  console.log('alert');
  return Alert.alert(
    'Alert Title',
    `${msg}`,
    [
      {
        text: 'Ask me later',
        onPress: () => console.log('Ask me later pressed'),
      },
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {text: 'OK', onPress: () => console.log('OK Pressed')},
    ],
    {cancelable: false},
  );
};
export {AlertBox};
