import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';

export default class Block extends Component {
  render() {
    const {
      flex,
      row,
      center,
      middle,
      right,
      bottom,
      space,
      style,
      children,
      ...props
    } = this.props;
    const _styles = [
      flex && {flex},
      flex === 'disabled' && {flex: 0},
      center && styles.center,
      bottom && styles.bottom,
      middle && styles.middle,
      right && styles.right,
      space && {justifyContent: `space-${space}`},
      row && styles.row,

      style,
    ];
    return (
      <View style={_styles} {...props}>
        {children}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
  },
  center: {
    alignItems: 'center',
  },
  middle: {
    justifyContent: 'center',
  },
  right: {
    justifyContent: 'flex-end',
  },
  bottom: {
    alignItems: 'flex-end',
  },
});
