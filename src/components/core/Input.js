import React, {Component} from 'react';
import {StyleSheet, View, Image, TextInput, Dimensions} from 'react-native';
import {Text, Block} from './index';
import * as theme from '../../constants/theme';

const {width} = Dimensions.get('window');

export default class Input extends Component {
  render() {
    const {
      label,
      rightLabel,
      full,
      email,
      phone,
      number,
      password,
      iconSource,
      errorIconSource,
      primaryFont,
      h1,
      h2,
      h3,
      h4,
      h5,
      h6,
      style,
      ...props
    } = this.props;

    const containerStyles = [full && styles.full, styles.container, style];

    const inputStyles = [
      primaryFont && styles.font,
      h1 && styles.h1,
      h2 && styles.h2,
      h3 && styles.h3,
      h4 && styles.h4,
      h5 && styles.h5,
      h6 && styles.h6,
      styles.input,

      // full && styles.full,
    ];

    const inputType = email
      ? 'email-address'
      : number
      ? 'numeric'
      : phone
      ? 'phone-pad'
      : 'default';

    let borderBottomColor = {
      borderBottomColor: props.error ? theme.colors.red : theme.colors.border,
    };
    let icon = iconSource ? (
      <Image
        source={iconSource}
        style={{
          width: 25,
          height: 25,
          resizeMode: 'stretch',
          marginRight: 10,
          tintColor: theme.colors.gray,
        }}
      />
    ) : null;
    let errorIcon =
      errorIconSource && props.error ? (
        <Image
          source={errorIconSource}
          style={{
            width: 20,
            height: 20,
            resizeMode: 'stretch',
            marginLeft: 5,
            tintColor: theme.colors.red,
          }}
        />
      ) : null;

    return (
      <View>
        <Block row center middle style={[containerStyles, borderBottomColor]}>
          {icon}
          <TextInput
            style={inputStyles}
            secureTextEntry={password}
            autoCapitalize="none"
            autoCorrect={false}
            keyboardType={inputType}
            {...props}
          />
          {/* ERROR ICON WHICH SHOWEN RIGHT OF INPUT */}
          {errorIcon}
        </Block>

        {/* ERROR MESSAGE WHICH SHOWEN UNDER OF INPUT */}
        {props.error ? (
          <Text right paragraph="sm" color="red" style={{paddingRight: 10}}>
            {props.error}
          </Text>
        ) : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    borderBottomWidth: 2,
  },
  h1: {fontSize: theme.fontSizes.h1.fontSize},
  h2: {fontSize: theme.fontSizes.h2.fontSize},
  h3: {fontSize: theme.fontSizes.h3.fontSize},
  h4: {fontSize: theme.fontSizes.h4.fontSize},
  h5: {fontSize: theme.fontSizes.h5.fontSize},
  h6: {fontSize: theme.fontSizes.h6.fontSize},
  input: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',

    // fontSize: theme.fontSizes.h4.fontSize,
    color: theme.colors.black,
  },
  font: {
    fontFamily: theme.primaryFontFamily,
  },
  full: {
    width: width - 50,
  },
  icon: {},
});
