import React, {Component} from 'react';
import {StyleSheet, Text} from 'react-native';
import * as theme from '../../constants/theme';

export default class Typography extends Component {
  render() {
    const {
      center,
      right,
      left,
      color,
      button,
      h1,
      h2,
      h3,
      h4,
      h5,
      h6,
      opacity,
      caption,
      paragraph,
      bold,
      style,
      children,
      fontFamily,
      ...props
    } = this.props;

    const _styles = [
      button && theme.fonts.button,
      h1 && styles.h1,
      h2 && styles.h2,
      h3 && styles.h3,
      h4 && styles.h4,
      h5 && styles.h5,
      h6 && styles.h6,
      fontFamily && fontFamily === 'main',
      caption && caption === 'sm' && theme.fonts.caption.sm,
      caption && caption === 'md' && theme.fonts.caption.md,
      caption && caption === 'lg' && theme.fonts.caption.lg,
      caption && caption === 'xlg' && theme.fonts.caption.xlg,
      caption && caption === 'logo' && theme.fonts.caption.logo,
      paragraph && paragraph === 'sm' && theme.fonts.paragraph.sm,
      paragraph && paragraph === 'md' && theme.fonts.paragraph.md,
      paragraph && paragraph === 'lg' && theme.fonts.paragraph.lg,
      opacity && {opacity},
      color && color === 'blue' && styles.blue,
      color && color === 'lightblue' && styles.lightblue,
      color && color === 'green' && styles.green,
      color && color === 'red' && styles.red,
      color && color === 'yellow' && styles.yellow,
      color && color === 'teal' && styles.teal,
      color && color === 'black' && styles.black,
      color && color === 'black2' && styles.black2,
      color && color === 'black3' && styles.black3,
      color && color === 'white' && styles.white,
      color && color === 'gray' && styles.gray,
      color && color === 'gray2' && styles.gray2,
      color && color === 'gray3' && styles.gray3,
      color && color === 'caption' && styles.caption,

      bold && styles.bold,
      center && styles.center,
      right && styles.right,
      left && styles.left,
      style,
    ];

    return (
      <Text style={_styles} {...props}>
        {children}
      </Text>
    );
  }
}
const styles = StyleSheet.create({
  center: {textAlign: 'center'},
  right: {textAlign: 'right'},
  left: {textAlign: 'left'},
  h1: {fontSize: theme.fontSizes.h1.fontSize},
  h2: {fontSize: theme.fontSizes.h2.fontSize},
  h3: {fontSize: theme.fontSizes.h3.fontSize},
  h4: {fontSize: theme.fontSizes.h4.fontSize},
  h5: {fontSize: theme.fontSizes.h5.fontSize},
  h6: {fontSize: theme.fontSizes.h6.fontSize},
  fontFamily: {fontFamily: theme.primaryFontFamily},
  bold: {fontWeight: 'bold'},
  blue: {color: theme.colors.blue},
  lightblue: {color: theme.colors.lightblue},
  green: {color: theme.colors.green},
  red: {color: theme.colors.red},
  yellow: {color: theme.colors.yellow},
  teal: {color: theme.colors.teal},
  black: {color: theme.colors.black},
  black2: {color: theme.colors.black2},
  black3: {color: theme.colors.black3},
  white: {color: theme.colors.white},
  gray: {color: theme.colors.gray},
  gray2: {color: theme.colors.gray2},
  gray3: {color: theme.colors.gray3},
  caption: {color: theme.colors.caption},
});
