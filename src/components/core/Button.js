import React, {Component} from 'react';
import {
  TouchableOpacity,
  TouchableHighlight,
  TouchableWithoutFeedback,
  Dimensions,
  StyleSheet,
} from 'react-native';
import * as theme from '../../constants/theme';

const {width} = Dimensions.get('window');

export default class Button extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      style,
      children,
      shadow,
      type,
      full,
      backgroundColor,
      borderRadius,
      ...props
    } = this.props;

    const _styles = [
      styles.button,
      style,
      full && styles.full,
      shadow && shadow === 'low' && theme.shadows.low,
      backgroundColor &&
        backgroundColor === 'red' &&
        theme.backgroundColors.red,
      backgroundColor &&
        backgroundColor === 'green' &&
        theme.backgroundColors.green,
      backgroundColor &&
        backgroundColor === 'blue' &&
        theme.backgroundColors.blue,
      backgroundColor &&
        backgroundColor === 'default' &&
        theme.backgroundColors.default,
      borderRadius && borderRadius === 'xxs' && theme.borderRadiuses.xxs,
      borderRadius && borderRadius === 'xs' && theme.borderRadiuses.xs,
      borderRadius && borderRadius === 'sm' && theme.borderRadiuses.sm,
      borderRadius && borderRadius === 'md' && theme.borderRadiuses.md,
      borderRadius && borderRadius === 'lg' && theme.borderRadiuses.lg,
      borderRadius && borderRadius === 'xlg' && theme.borderRadiuses.xlg,
    ];

    const btn =
      type == 'WithoutFeedback' ? (
        <TouchableWithoutFeedback style={_styles} {...props}>
          {children}
        </TouchableWithoutFeedback>
      ) : (
        <TouchableOpacity style={_styles} activeOpacity={0.8} {...props}>
          {children}
        </TouchableOpacity>
      );
    return btn;
  }
}

const styles = StyleSheet.create({
  button: {
    // height: 55,

    alignItems: 'center',
    justifyContent: 'center',
  },
  full: {
    width: width - 50,
  },
});
