import React, {Component} from 'react';
import {FlatList, Image, StyleSheet} from 'react-native';
import {Text, Block, List} from './';
import {DIMENSIONS} from '../../constants/theme';

export default class PostItem extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const {data, flex, numColumns, ...props} = this.props;
    const size = DIMENSIONS.WINDOW_WIDTH / numColumns;
    // const _styles = [flex && {flex}, flex === 'disabled' && {flex: 0}];

    return (
      <Block {...props}>
        <Image
          resizeMode="contain"
          style={styles.image}
          width={size}
          source={{uri: data.images[0].uri}}
        />
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  image: {
    backgroundColor: '#D8D8D8',
    aspectRatio: 1,
  },
});
